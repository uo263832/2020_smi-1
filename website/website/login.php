<?php include('servidor.php') ?>

<?php if (!isset($_SESSION['username'])) : ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>TUCINE</title>
<!-- 

Highway Template

https://templatemo.com/tm-520-highway

-->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/light-box.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Kanit:100,200,300,400,500,600,700,800,900" rel="stylesheet">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="icon" type="image/png" href="img/LOGO_TUCINE.png" />
    </head>

<body>

    <nav>
        <div class="logo">
            <a href="index.php">TU<em>CINE</em></a>
        </div>
        <div class="menu-icon">
        <span></span>
      </div>
    </nav>

    <div id="video-container">
        <div class="video-overlay"></div>
        <div class="video-content">
            <div class="inner">
              <h1>ACCEDE O REGÍSTRATE YA <em> </em></h1>
              <p></p>
              
                <div class="scroll-icon">
                    <a class="scrollTo" data-scrollTo="portfolio" href="#"><img src="img/scroll-icon.png" alt=""></a>
                </div>    
            </div>
        </div>
        <div >
      <img class="TUCINE_LOGO" src="img/LOGO_TUCINE.png" >
    </div> 
        <!--<video controls autoplay>
          <source src="MI.mp4" type="video/mp4">
        </video> --> 
      </div>
    </div>


    <div class="full-screen-portfolio" id="portfolio">
        <div class="container-fluid">
        <!-- Modal -->
    
      <!-- Modal Content -->
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h3 class="header-title"><em></em></h3>
             
        </div>
        <!-- Modal Body -->
        <div class="modal-body">
          <div class="col-md-6 col-md-offset-3">
            <form id="contact" action="login.php" method="post" autocomplete="off">
              <?php include('errors.php') ?>
                <div class="row">
                    <div class="col-md-12">
                      <fieldset>
                        <input name="usuario" type="text" class="form-control" id="name" placeholder="Nombre..." required="">
                      </fieldset>
                    </div>
                    
                    <div class="col-md-12">
                      <fieldset>
                        <input name="contraseña" type="password" class="form-control" id="name" placeholder="Contraseña..." required="">
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <button type="submit" name="login" id="form-submit" class="btn">Log In</button>
                      </fieldset>
                    </div>
                    <p style="color:#FFFFFF"> ¿No eres usuario? <a href="registro_2.php"><b> Regístrate</b></a></p>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    
            
            
           
             
            
        
        



        </div>
    </div>


    <footer>
        <div class="container-fluid">
            <div class="col-md-12">
                <p>Copyright &copy; 2020 TUCINE 
    
    | Designed by 2020_SMI-1</p>
            </div>
        </div>
    </footer>


      <!-- Modal button -->
    <!--<div class="popup-icon">
      <button id="modBtn" class="modal-btn"><img src="img/contact-icon.png" alt=""></button>
    </div>  
-->
    
    

    <section class="overlay-menu">
      <div class="container">
        <div class="row">
          <div class="main-menu">
              <ul>
                  <li>
                      <a href="index.php">Inicio</a>
                  </li>
                 
                  <li>
                      <a href="subirvideo.php">Sube tu vídeo</a>
                  </li>
                  <li>
                      <a href="login.php">Log In</a>
                  </li>
                  
                  <!--<li>
                      <a href="single-post.html">Ayuda</a>
                  </li> -->
              </ul>
              <p></p>
          </div>
        </div>
      </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>
    
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    
</body>
</html>



<?php endif ?>


<?php if(isset($_SESSION['username'])) : ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>TUCINE</title>
<!-- 

Highway Template

https://templatemo.com/tm-520-highway

-->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/light-box.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Kanit:100,200,300,400,500,600,700,800,900" rel="stylesheet">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="icon" type="image/png" href="img/LOGO_TUCINE.png" />
    </head>

<body>

    <nav>
        <div class="logo">
            
             <a href="index.php">TU<em>CINE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></a>
           <a>BIENVENIDO &nbsp;<em><?php echo $_SESSION['username']; ?></em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
           <a href="logout.php">Logout</a>
        </div>
        <div class="menu-icon">
        <span></span>
      </div>
    </nav>

    <div id="video-container">
        <div class="video-overlay"></div>
        <div class="video-content">
            <div class="inner">
              <h1>ACCEDE O REGÍSTRATE YA <em> </em></h1>
              <p></p>
              
                <div class="scroll-icon">
                    <a class="scrollTo" data-scrollTo="portfolio" href="#"><img src="img/scroll-icon.png" alt=""></a>
                </div>    
            </div>
        </div>
        <div >
      <img class="TUCINE_LOGO" src="img/LOGO_TUCINE.png" >
    </div> 
        <!--<video controls autoplay>
          <source src="MI.mp4" type="video/mp4">
        </video> --> 
      </div>
    </div>


    <div class="full-screen-portfolio" id="portfolio">
        <div class="container-fluid">
        <!-- Modal -->
    
      <!-- Modal Content -->
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h3 class="header-title"><em></em></h3>
             
        </div>
        <!-- Modal Body -->
        <div class="modal-body">
          <div class="col-md-6 col-md-offset-3">
            <form id="contact" action="login.php" method="post" autocomplete="off">
              <?php include('errors.php') ?>
                <div class="row">
                    <div class="col-md-12">
                      <fieldset>
                        <input name="usuario" type="text" class="form-control" id="name" placeholder="Nombre..." required="">
                      </fieldset>
                    </div>
                    
                    <div class="col-md-12">
                      <fieldset>
                        <input name="contraseña" type="password" class="form-control" id="name" placeholder="Contraseña..." required="">
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <button type="submit" name="login" id="form-submit" class="btn">Log In</button>
                      </fieldset>
                    </div>
                    <p style="color:#FFFFFF"> ¿No eres usuario? <a href="registro_2.php"><b> Regístrate</b></a></p>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    
            
            
           
             
            
        
        



        </div>
    </div>


    <footer>
        <div class="container-fluid">
            <div class="col-md-12">
                <p>Copyright &copy; 2020 TUCINE 
    
    | Designed by 2020_SMI-1</p>
            </div>
        </div>
    </footer>


      <!-- Modal button -->
    <!--<div class="popup-icon">
      <button id="modBtn" class="modal-btn"><img src="img/contact-icon.png" alt=""></button>
    </div>  
-->
    
    

    <section class="overlay-menu">
      <div class="container">
        <div class="row">
          <div class="main-menu">
              <ul>
                  <li>
                      <a href="index.php">Inicio</a>
                  </li>
                 
                  <li>
                      <a href="subirvideo.php">Sube tu vídeo</a>
                  </li>
                  <li>
                      <a href="login.php">Log In</a>
                  </li>
             
                  <!--<li>
                      <a href="single-post.html">Ayuda</a>
                  </li> -->
              </ul>
              <p></p>
          </div>
        </div>
      </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>
    
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    
</body>
</html>

<?php endif ?>